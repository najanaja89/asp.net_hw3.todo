﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace asp.net_hw3.Todo.Models
{
    public class TodoesController : Controller
    {
        private TodoDataContext db = new TodoDataContext();

        // GET: Todoes
        public ActionResult Index()
        {
            var result = db.Todos.Select(t => new TodoTagsList
            {
                Id= t.Id,
                Name = t.Name,
                Description = t.Description,
                Tags = t.Tags.Select(x => x.TagName).ToList()
            }).ToList();

            return View(result);
        }

        [HttpPost]
        public ActionResult Index(string searchIndex)
        {
            var result = db.Todos.Select(t => new TodoTagsList
            {
                Id = t.Id,
                Name = t.Name,
                Description = t.Description,
                Tags = t.Tags.Select(x => x.TagName).ToList()
            }).Where(i=>i.Name.Contains(searchIndex)).ToList();

            return View(result);
        }

        // GET: Todoes/Details/5

        public ActionResult AddTag(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Todo todo = db.Todos.Find(id);
            var todoViewModel = new TodoViewModel
            { 
                Id = todo.Id,
                Name = todo.Name,
                Description= todo.Description
            };
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todoViewModel);
        }

        [HttpPost]
        public ActionResult AddTag(TodoViewModel todoViewModel)
        {
            var tag = new Tag
            {
                TagName = todoViewModel.Tag.TagName
            };

            tag.Todos.Add(db.Todos.Find(todoViewModel.Id));
            db.Tags.Add(tag);
            db.Entry(db.Todos.Find(todoViewModel.Id)).State = EntityState.Modified;
            
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Todo todo = db.Todos.Find(id);
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todo);
        }

        // GET: Todoes/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        public ActionResult Create()
        {
            //var todoViewModel = new TodoViewModel();
            //Todo todo = new Todo();

            return View();
        }

        // POST: Todoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TodoViewModel todoViewModel)
        {

            if (ModelState.IsValid)
            {
                var tag = new Tag
                {
                    TagName = todoViewModel.Tag.TagName
                };
                var todo = new Todo
                {
                    Name = todoViewModel.Name,
                    Description = todoViewModel.Description
                };
                tag.Todos.Add(todo);
                todo.Tags.Add(tag);


                db.Todos.Add(todo);
                db.Tags.Add(tag);

                db.Tags.Add(tag);
                db.Todos.Add(todo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(todoViewModel);
        }

        // GET: Todoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Todo todo = db.Todos.Find(id);
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todo);
        }

        // POST: Todoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description")] Todo todo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(todo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(todo);
        }

        // GET: Todoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Todo todo = db.Todos.Find(id);
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todo);
        }

        // POST: Todoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Todo todo = db.Todos.Find(id);
            db.Todos.Remove(todo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
